#include <iostream>

#include "PrintOperator.h"

PrintOperator::PrintOperator(
        const std::string &id,
        xmp::core::OperatorPriority priority,
        const xmp::core::IOperatorParameters *parameters,
        xmp::core::IContext *context)
        : xmp::plugin::OperatorBase(id, priority, context)
{
    in = registerInput("in");
}

void PrintOperator::execute()
{
    xmp::core::IBuffer *buffer = in->read();
    if (buffer) {
        for (int i = 0; i < buffer->size(); i++) {
            std::cout << buffer->at(i);
        }
        delete buffer;
        std::cout << std::endl;
    }
}

bool PrintOperator::start()
{
    return true;
}

bool PrintOperator::stop()
{
    return true;
}
