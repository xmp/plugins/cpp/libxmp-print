#ifndef XMP_PRINTER_OPERATOR
#define XMP_PRINTER_OPERATOR

#include <xmp/plugin/cpp/OperatorBase.h>
#include <xmp/core/operator/OperatorParameters.h>

class PrintOperator : public xmp::plugin::OperatorBase
{
public:
    PrintOperator(
            std::string const &id,
            xmp::core::OperatorPriority priority,
            xmp::core::IOperatorParameters const *parameters,
            xmp::core::IContext *context);

    void execute();

    bool start();
    bool stop();

private:
    xmp::core::IInput *in;
};

#endif // XMP_PRINTER_OPERATOR
