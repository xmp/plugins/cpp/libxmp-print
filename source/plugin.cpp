#include <xmp/plugin/cpp/plugin.h>

#include <xmp/plugin/cpp/GenericOperatorCreator.h>

#include "PrintOperator.h"

void registerCreators(xmp::core::IOperatorStore *store)
{
    store->registerCreator("print", new xmp::plugin::GenericOperatorCreator<PrintOperator>("print"));
}
